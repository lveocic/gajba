﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gajba.Model.Common.Models
{
    public interface IBrewery
    {
        #region Properties

        string Abrv { get; set; }
        string Address { get; set; }
        IEnumerable<IBeer> Beers { get; set; }
        string Country { get; set; }
        DateTime DateCreated { get; set; }
        DateTime DateModified { get; set; }
        string Est { get; set; }
        Guid Id { get; set; }
        string Name { get; set; }

        #endregion Properties
    }
}