﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gajba.Model.Common.Models
{
    public interface IBeer
    {
        #region Properties

        string Abrv { get; set; }
        int BeerType { get; set; }
        IBrewery Brewery { get; set; }
        Guid BreweryId { get; set; }
        DateTime DateCreated { get; set; }
        DateTime DateModified { get; set; }
        Guid Id { get; set; }
        bool IsCraft { get; set; }
        string Name { get; set; }

        #endregion Properties
    }
}