﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gajba.DAL.Entities
{
    public class BreweryEntity //klasa sa informacijama o stupcima u relacijskoj bazi podataka
    {
        #region Properties

        public string Abrv { get; set; }
        public string Address { get; set; }
        public List<BeerEntity> Beers { get; set; }
        public string Country { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public string Est { get; set; }
        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion Properties
    }
}