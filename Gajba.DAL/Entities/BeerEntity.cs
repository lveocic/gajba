﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gajba.DAL.Entities
{
    public class BeerEntity //klasa sa svim stupcima koji nam se nalaze u relacijskoj bazi podataka
    {
        #region Properties

        public string Abrv { get; set; }
        public int BeerType { get; set; }
        public BreweryEntity Brewery { get; set; }
        public Guid BreweryId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public Guid Id { get; set; }
        public bool IsCraft { get; set; }
        public string Name { get; set; }

        #endregion Properties
    }
}