﻿using Gajba.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace Gajba.DAL
{
    public class GajbaContext : DbContext
    //klasa GajbaContext nasljeduje DbContext klasu koja je predefinirana EntityFrameworkCore klasa
    //omogucava nam rad sa bazom podataka
    {
        #region Constructors

        public GajbaContext(DbContextOptions<GajbaContext> options) //konstruktor kojemu predajem objekt tipa DbContextOptions (Microsoft EntityFrameworkCore klasa) od GajbaContext klase
             : base(options)
        {
            Database.EnsureCreated();
        }

        #endregion Constructors

        #region Properties

        public DbSet<BeerEntity> Beers { get; set; }

        public DbSet<BreweryEntity> Breweries { get; set; }

        #endregion Properties

        #region Methods

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseSerialColumns();
        }

        #endregion Methods
    }
}