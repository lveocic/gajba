﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Gajba.DAL.Migrations
{
    public partial class update452022 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BeerEntityBreweryEntity");

            migrationBuilder.CreateIndex(
                name: "IX_Beers_BreweryId",
                table: "Beers",
                column: "BreweryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Beers_Breweries_BreweryId",
                table: "Beers",
                column: "BreweryId",
                principalTable: "Breweries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Beers_Breweries_BreweryId",
                table: "Beers");

            migrationBuilder.DropIndex(
                name: "IX_Beers_BreweryId",
                table: "Beers");

            migrationBuilder.CreateTable(
                name: "BeerEntityBreweryEntity",
                columns: table => new
                {
                    BeersId = table.Column<Guid>(type: "uuid", nullable: false),
                    BreweryId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BeerEntityBreweryEntity", x => new { x.BeersId, x.BreweryId });
                    table.ForeignKey(
                        name: "FK_BeerEntityBreweryEntity_Beers_BeersId",
                        column: x => x.BeersId,
                        principalTable: "Beers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BeerEntityBreweryEntity_Breweries_BreweryId",
                        column: x => x.BreweryId,
                        principalTable: "Breweries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BeerEntityBreweryEntity_BreweryId",
                table: "BeerEntityBreweryEntity",
                column: "BreweryId");
        }
    }
}
