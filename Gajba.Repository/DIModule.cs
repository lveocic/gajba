﻿using Autofac;
using Gajba.Repository.Common.Repositories;
using Gajba.Repository.Repositories;

namespace Gajba.Repository
{
    public class DIModule : Module
    {
        #region Methods

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<BeerRepository>().As<IBeerRepository>();
            builder.RegisterType<BreweryRepository>().As<IBreweryRepository>();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();
            builder.RegisterTypes(typeof(IRepository<,,>), typeof(Repository<,,>));
        }

        #endregion Methods
    }
}