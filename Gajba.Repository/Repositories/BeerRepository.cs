﻿using AutoMapper;
using Gajba.Common.FilterParameters;
using Gajba.DAL;
using Gajba.DAL.Entities;
using Gajba.Model.Common.Models;
using Gajba.Model.Models;
using Gajba.Repository.Common.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gajba.Repository.Repositories
{
    public class BeerRepository : Repository<BeerEntity, IBeer, IBeerFilter>, IBeerRepository //klasa koja nasljeduje Repository apstraktnu klasu i implementira IBeerRepository interface
    {
        #region Constructors

        public BeerRepository(
            GajbaContext context,
            IMapper mapper) : base(context, mapper) //konstruktor kojemu predajem objekt tipa GajbaContext koji nam omogucuje uredivanje baze, mappera koji mapira iz klase u klasu (u ovom slućaju iz klase Beer Model u BeerEntity)
        {
        }

        #endregion Constructors

        #region Methods

        protected override Task<IQueryable<BeerEntity>> ApplyFilteringAsync(IQueryable<BeerEntity> query, IBeerFilter filter)
        //metoda prava pristupa protected zbog zastite koristenja, ali sa ostavljenom mogucnoscu za daljnjim nasljedivanjem,
        //Task je zato sto je rijec o asinkronoj metodi, predajemo IQueryable listu (radimo direktno na bazi) Beer entitya
        //parametri koje metoda prima su BeerEntity tipa IQueryable i filter tipa IBeerFilter
        {
            if (filter != null) //provjera jeli objekt null
            {
                if (!string.IsNullOrWhiteSpace(filter.SearchQuery)) //ako objekt nije null i pretraga nije null ili je nema
                {
                    query = query.Where(x => x.Name.ToLower().Contains(filter.SearchQuery.ToLower())); //filtrira po zadanim parametrima
                }
                if (filter.Ids != null && filter.Ids.Any()) //ukoliko u filteru lista id-jeva nije null i ukoliko ima vrijednosti
                {
                    query = query.Where(x => filter.Ids.Contains(x.Id)); //filtrira po zadanim parametrima
                }
                if (filter.IsCraft.HasValue) //ukoliko IsCraft property ima vrijednost
                {
                    query = query.Where(x => x.IsCraft == filter.IsCraft.Value); //filtrira
                }
                if (filter.BreweryIds != null && filter.BreweryIds.Any()) //ukoliko lista id-jeva nije null i postoji barem jedan element u listi
                {
                    query = query.Where(x => filter.BreweryIds.Contains(x.BreweryId)); //filtrira
                }
            }
            return Task.FromResult(query); //vraca dopunjeni query
        }

        protected override Task<IQueryable<BeerEntity>> ApplyPagingAsync(IQueryable<BeerEntity> query, IBeerFilter filter)
        //metoda prava pristupa protected zbog zastite uredivanja, ali sa ostavljenom mogucnoscu za daljnjim nasljedivanjem,
        //Task je zato sto je rijec o asinkronoj metodi, predajemo IQueryable listu (radimo direktno na bazi) Beer entitya
        //parametri koje metoda prima su BeerEntity tipa IQueryable i filter tipa IBeerFilter
        {
            if (filter != null) //ukoliko objekt nije null
            {
                if (filter.Page.HasValue && filter.PageSize.HasValue) //provjerava imamo li postavljen paging
                {
                    query = query.Skip((filter.Page.Value - 1) * filter.PageSize.Value).Take(filter.PageSize.Value); //postavlja paging
                }
            }
            return Task.FromResult(query); //vraca dopunjen query
        }

        protected override Task<IQueryable<BeerEntity>> ApplySortingAsync(IQueryable<BeerEntity> query, IBeerFilter filter)
        //metoda prava pristupa protected zbog zastite uredivanja, ali sa ostavljenom mogucnoscu za daljnjim nasljedivanjem,
        //Task je zato sto je rijec o asinkronoj metodi, predajemo IQueryable listu (radimo direktno na bazi) Beer entitya
        //parametri koje metoda prima su BeerEntity tipa IQueryable i filter tipa IBeerFilter
        {
            if (!string.IsNullOrWhiteSpace(filter.OrderBy) && !string.IsNullOrWhiteSpace(filter.OrderDirection)) //ukoliko objekti zadovoljavaju uvjete
            {
                if (filter.OrderBy == nameof(IBeer.Name)) //provjeravamo jeli orderby parametar koji trazimo
                {
                    query = filter.OrderDirection == "asc" ? query.OrderBy(x => x.Name) : query.OrderByDescending(x => x.Name); //ternarni operator filtira po imenu objekta
                }
                else if (filter.OrderBy == nameof(IBeer.BeerType)) //filtrira po vrstu objekta
                {
                    query = filter.OrderDirection == "asc" ? query.OrderBy(x => x.BeerType) : query.OrderByDescending(x => x.BeerType); //ternarni operator filtrira po vrsti objekta
                }
            }
            return Task.FromResult(query); //vraca dopunjen query
        }

        #endregion Methods
    }
}