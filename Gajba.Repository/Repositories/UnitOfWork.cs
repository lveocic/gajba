﻿using AutoMapper;
using Gajba.DAL;
using Gajba.Repository.Common.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gajba.Repository.Repositories
{
    public class UnitOfWork : IUnitOfWork //UnitOfWork klasa nam omogucava visestruko predavanje zahtjeva na bazu u jednoj transakciji, implementira interface
    {
        #region Fields

        private readonly GajbaContext Context; //private je da mu ne moze pristupiti nitko van UOW-a, readonly je da nije podlozan izmjenama, GajbaContext klasa nam sluzi za komunikaciju s bazom
        private IBeerRepository beerRepository;
        private IBreweryRepository breweryRepository;

        #endregion Fields

        #region Constructors

        public UnitOfWork(GajbaContext context, IMapper mapper) //konstruktor kojemu predajem objekte tipa GajbaContext i IMapper
        {
            Context = context;
            Mapper = mapper;
        }

        #endregion Constructors

        #region Properties

        public IBeerRepository BeerRepository => beerRepository ?? new BeerRepository(Context, Mapper); //ternarni operator koji provjerava jel imamo beerRepository ako nemamo napravit ce novi
        public IBreweryRepository BreweryRepository => breweryRepository ?? new BreweryRepository(Context, Mapper); //ternarni operator koji provjerava jel imamo breweryRepository ako nemamo napravit ce novi
        protected IMapper Mapper { get; private set; }

        #endregion Properties

        //private je set zbog toga sto se ne smije mjenjati

        #region Methods

        public async Task<bool> Complete() //metoda koja poziva spremanje promjena na bazi
        {
            return await Context.SaveChangesAsync() > 0; //ako je broj promjena veci od 0 vraca true, u suprotnom false
        }

        public void Dispose() //metoda koja cisti iz memorije
        {
            Context.Dispose();
        }

        #endregion Methods
    }
}