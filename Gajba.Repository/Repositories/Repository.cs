﻿using AutoMapper;
using Gajba.Common.FilterParameters;
using Gajba.DAL;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Gajba.Repository.Repositories
{
    public class Repository<T, TModel, TFilter> : IRepository<T, TModel, TFilter> where T : class where TModel : class where TFilter : IFilter
        //klasa generickog repositorija koja radi sa generickim podacima T, TModel i TFilter gdje je T klasa(entity), TModel je klasa (model) i TFilter je IFilter tipa
        //nju ce nasljediti svi buduci konkretni repozitoriji sa konkretnim tipovima
    {
        #region Fields

        protected readonly GajbaContext Context;

        #endregion Fields

        //protected je kako se nebi mogao koristiti u vanjskim klasama koje koriste Repository,
        //ali ga mogu koristitit naslijedene klase
        //readonly je da nije podlozan izmjenama, GajbaContext klasa nam sluzi za komunikaciju s bazom

        #region Constructors

        public Repository(GajbaContext context, IMapper mapper) //konstruktor kojemu predajem objekte tipa GajbaContext i IMapper i instanciram ih
        {
            Context = context;
            Mapper = mapper;
        }

        #endregion Constructors

        #region Properties

        protected IMapper Mapper { get; private set; }

        #endregion Properties

        //property mapper ima private set zbog toga sto mu se nesmije mijenjati vrijednost van klase

        #region Methods

        public async Task<bool> AddAsync(TModel entity) //asinkrona metoda povratnog tipa bool predajem joj genericki objekt tipa TModel
        {
            try
            {
                if (await AddUOWAsync(entity)) //ako su zadovoljeni uvjeti metode
                    return await Context.SaveChangesAsync() > 0; //vraca true ako ima promjena na bazi

                return false; //ukoliko nema promjena na bazi vraca false
            }
            catch (Exception exception)
            {
                throw new Exception($"{this.ToString()} - insert failed", exception); //ukoliko program pukne throwa ovaj exception
            }
        }

        public async Task<bool> AddUOWAsync(TModel entity) //asinkrona metoda povratnog tipa bool predajem joj objekt tipa TModel
        {
            if (entity == null) //ukoliko je objekt null
                return false; //vraca false

            await Context.Set<T>().AddAsync(Mapper.Map<T>(entity)); //mapira model u entity te poziva metodu koja salje promjenu (dodavanje) na bazu
            return true; //vraca true
        }

        public async Task<bool> AddUOWRangeAsync(IEnumerable<TModel> entities) //asinkrona metoda povratnog tipa bool zaprima IEnumerable listu objekata tipa TModel
        {
            if (entities == null || !entities.Any()) //provjerava jesu li objetki null i ima li ih
                return false; //ako su null i nema ih vraca false

            await Context.Set<T>().AddRangeAsync(Mapper.Map<IEnumerable<T>>(entities)); //mapira listu modela na listu entityja i poziva metodu koja salje promjene na bazu
            return true; //vraca true
        }

        public async Task<bool> DeleteAsync(TModel entity) //asinkrona metoda povratnog tipa bool prima objekt tipa TModel
        {
            try
            {
                if (await DeleteUOWAsync(entity)) //izvrsava metodu
                    return await Context.SaveChangesAsync() > 0; //ako ima promjena na bazi vraca true
                return false; //ukoliko nisu vraca false
            }
            catch (Exception exception)
            {
                throw new Exception($"{this.ToString()} - delete failed", exception);
            }
        }

        public Task<bool> DeleteRangeUOWAsync(IEnumerable<TModel> entities) //metoda povratnog tipa bool prima IEnumerable listu tipa TModel objekata
        {
            if (entities == null || !entities.Any()) //provjerava uvijet
                return Task.FromResult(false); //povratna info false

            Context.Set<T>().RemoveRange(Mapper.Map<IEnumerable<T>>(entities)); //mapira listu modela na listu entityja te salje poziv za brisanje na bazu
            return Task.FromResult(true); //povratna info true
        }

        public Task<bool> DeleteUOWAsync(TModel entity) //metoda povratnog tipa bool, prima objekt tipa TModel
        {
            if (entity == null) //ukoliko je objekt null
                return Task.FromResult(false); //vraca false

            Context.Set<T>().Remove(Mapper.Map<T>(entity)); //mapira model na entity i salje brisanje na bazu
            return Task.FromResult(true); //vraca true
        }

        public async Task<IEnumerable<TModel>> FindAsync(TFilter filter) //asinkrona metoda povratnog tipa TModel IEnumerable, zaprima objekt tipa TFilter
        {
            try
            {
                IQueryable<T> query = Context.Set<T>(); //postavlja IQueryable listu za rad na bazi
                query = await ApplyFilteringAsync(query, filter); //filtrira
                query = await ApplySortingAsync(query, filter); //sortira
                query = await ApplyPagingAsync(query, filter); //radi paging
                return Mapper.Map<IEnumerable<TModel>>(await query.ToListAsync()); //obavlja sve prethodne korake na bazi te mapira listu entityja na listu modela
            }
            catch (Exception exception)
            {
                throw new Exception($"{this.ToString()} - find failed", exception);
            }
        }

        public async Task<IEnumerable<TModel>> GetAllAsync() //asinkrona metoda vraca TModel IEnumerable listu
        {
            return Mapper.Map<IEnumerable<TModel>>(await Context.Set<T>().ToListAsync()); //dohvaca sve stvarne objekte, mapira i vraca natrag
        }

        public async Task<TModel> GetAsync(Guid id) //asinkrona metoda vraca objekt tipa TModel zaprima objekt tipa Guid
        {
            return Mapper.Map<TModel>(await Context.Set<T>().FindAsync(id)); //vraca objekt pronaden pod predanim objektom tipa Guid
        }

        public async Task<bool> UpdateAsync(TModel entity) //asinkrona metoda povratnog tipa bool, predajemo objekt tipa TModel
        {
            try
            {
                if (await UpdateUOWAsync(entity)) //ukoliko je izvrsena metoda
                    return await Context.SaveChangesAsync() > 0; //vraca true ako ima promjena
                return false;//ukoliko nije, vraca false
            }
            catch (Exception exception)
            {
                throw new Exception($"{this.ToString()} - update failed", exception);
            }
        }

        public Task<bool> UpdateUOWAsync(TModel entity) //metoda povratnog tipa bool, predajemo objekt tipa TModel
        {
            if (entity == null) //ukoliko je objekt null
                return Task.FromResult(false); //vraca false
            Context.Set<T>().Update(Mapper.Map<T>(entity)); //ukoliko objekt nije null, mapira model u entity, poziva, salje promjenu na bazu
            return Task.FromResult(true); //vraca true
        }

        protected virtual Task<IQueryable<T>> ApplyFilteringAsync(IQueryable<T> query, TFilter filter)
        //metoda prava pristupa protected da joj nitko ne moze pristupiti, virtual je kako bi ju mogli overridati, dajemo joj objekt tipa IQueryable<T> i filter tipa TFilter
        {
            return Task.FromResult(query);
        }

        protected virtual Task<IQueryable<T>> ApplyPagingAsync(IQueryable<T> query, TFilter filter)
        //metoda prava pristupa protected da joj nitko ne moze pristupiti, virtual je kako bi ju mogli overridati, predajemo joj objekt tipa IQueryable<T> i filter tipa TFilter
        {
            return Task.FromResult(query);
        }

        protected virtual Task<IQueryable<T>> ApplySortingAsync(IQueryable<T> query, TFilter filter)
        //metoda prava pristupa protected da joj nitko ne moze pristupiti, virtual je kako bi ju mogli overridati, predajemo joj objekt tipa IQueryable<T> i filter tipa TFilter
        {
            return Task.FromResult(query);
        }

        #endregion Methods
    }
}