﻿using AutoMapper;
using Gajba.Common.FilterParameters;
using Gajba.DAL;
using Gajba.DAL.Entities;
using Gajba.Model.Common.Models;
using Gajba.Model.Models;
using Gajba.Repository.Common.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gajba.Repository.Repositories
{
    public class BreweryRepository : Repository<BreweryEntity, IBrewery, IBreweryFilter>, IBreweryRepository //klasa BreweryRepository nasljeduje Repository apstraktnu klasu
    {
        #region Constructors

        public BreweryRepository(GajbaContext context, IMapper mapper) : base(context, mapper) //konstruktor nasljedene klase kojem prosljedujem parametre
        {
        }

        #endregion Constructors

        #region Methods

        protected override Task<IQueryable<BreweryEntity>> ApplyFilteringAsync(IQueryable<BreweryEntity> query, IBreweryFilter filter)
        //metoda prava pristupa protected zbog zastite koristenja, ali sa ostavljenom mogucnoscu za daljnjim nasljedivanjem,
        //Task je zato sto je rijec o asinkronoj metodi, predajemo IQueryable listu (radimo direktno na bazi) Brewery entitya
        //parametri koje metoda prima su BreweryEntity tipa IQueryable i filter tipa IBreweryFilter
        {
            if (filter != null) //ukoliko objekt nije null
            {
                if (!string.IsNullOrWhiteSpace(filter.SearchQuery)) //ukoliko pretraga nije null niti je prazna
                {
                    query = query.Where(x => x.Name.ToLower().Contains(filter.SearchQuery.ToLower())); //filtrira po nazivu i pretrazeni objekt formatira u mala slova
                }
                if (filter.Ids != null && filter.Ids.Any()) //ukoliko u filteru lista id-jeva nije null i ukoliko ima vrijednosti
                {
                    query = query.Where(x => filter.Ids.Contains(x.Id)); //dodaje filtriranje po id-u
                }
            }
            return Task.FromResult(query); //vraca dopunjeni query filtera
        }

        #endregion Methods
    }
}