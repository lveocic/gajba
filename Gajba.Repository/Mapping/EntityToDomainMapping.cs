﻿using AutoMapper;
using Gajba.DAL.Entities;
using Gajba.Model.Common.Models;
using Gajba.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gajba.Repository.Mapping
{
    public class EntityToDomainMapping : Profile
    {
        #region Constructors

        public EntityToDomainMapping()
        {
            //Beer
            CreateMap<Beer, BeerEntity>().ReverseMap();
            CreateMap<IBeer, BeerEntity>().ReverseMap();

            //Brewery
            CreateMap<Brewery, BreweryEntity>();
            CreateMap<BreweryEntity, Brewery>();
            CreateMap<IBrewery, BreweryEntity>();
            CreateMap<BreweryEntity, IBrewery>();
        }

        #endregion Constructors
    }
}