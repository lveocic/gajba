﻿using Gajba.Common.FilterParameters;
using Gajba.DAL;
using Gajba.DAL.Entities;
using Gajba.Model.Common.Models;
using Gajba.Repository.Common.Repositories;
using Gajba.Repository.Repositories;
using Gajba.Service.Common.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gajba.Service.Services
{
    public class BeerService : IBeerService //klasa BeerService implementira IBeerService interface
    {
        #region Constructors

        public BeerService(IBeerRepository beerRepository) //konstruktoru predajemo objekt tipa IBeerRepository
        {
            BeerRepository = beerRepository;
        }

        #endregion Constructors

        #region Properties

        protected IBeerRepository BeerRepository { get; private set; }

        #endregion Properties

        //protected je da joj nitko ne moze pristupit, a private je set zbog toga sto se ne smije mjenjati

        #region Methods

        public async Task<bool> DeleteBeerAsync(Guid id)
        {
            var beer = await GetBeerAsync(id); //pozivam metodu za dohvacanje objekta predajem joj id objekta
            if (beer == null) //ako je objekt null dobit ću povratnu informaciju da nema objekta po tom id-u , false
                return false;
            var result = await BeerRepository.DeleteAsync(beer); //pozivam metodu iz repositoryja te joj predajem objekt i ona ga briše s baze
            return result;
        }

        public async Task<IEnumerable<IBeer>> FindBeersAsnyc(IBeerFilter filter) //pozivam metodu za pronalazak objekta, predajem joj objekt tipa IBeerFilter
        {
            return await BeerRepository.FindAsync(filter); //pozivam metodu sa repositoryja predajem joj objekt i ona ga pronalazi
        }

        public async Task<IEnumerable<IBeer>> GetAllBeersAsync() //metoda dohvaca sve objekte sa baze
        {
            var result = await BeerRepository.GetAllAsync(); //pozivam metodu sa repositoryja i ona dohvaca trazeno
            return result;
        }

        public async Task<IBeer> GetBeerAsync(Guid id) //metoda dohvaca jedan objekt po id-u tipa Guid
        {
            var result = await BeerRepository.GetAsync(id); //pozivam metodu sa repositoryja i ona dohvaca trazeno
            return result;
        }

        public async Task<bool> InsertBeerAsync(IBeer beer) //metoda ubacuje na bazu objekt tipa IBeer
        {
            CreateBeer(beer); //izvrsava se metoda CreateBeer sa predanim objektom
            var result = await BeerRepository.AddAsync(beer); //pozivam metodu sa repositoryja i ona odraduje dalje
            return result; //ukoliko je sve ok, vraca nam true
        }

        public async Task<bool> UpdateBeerAsync(IBeer beer) //metoda azurira objekte, predajemo joj objekt tipa IBeer
        {
            beer.DateModified = DateTime.Now; //postavlja vrijeme azuriranja
            var result = await BeerRepository.UpdateAsync(beer); //pozivam metodu iz repositoryja i ona azurira objekt
            return result; //vraca nam povratnu info
        }

        private void CreateBeer(IBeer beer) //metoda je private jer se ne smije mijenjati, predajemo joj objekt tipa IBeer
        {
            beer.DateCreated = DateTime.UtcNow; //upisuje vrijeme kreiranja objekta
            beer.DateModified = DateTime.UtcNow; //upisuje vrijeme azuriranja objekta
            beer.Id = Guid.NewGuid(); //dodjeljuje mu Primary Key
            beer.Abrv = beer.Name.ToLower().Replace(" ", "-").Replace("č", "c").Replace("ć", "c").Replace("ž", "z").Replace("š", "s").Replace("đ", "d"); //postavlja abrv, prebacuje sve u mala slova, uklanja palatale i mice razmak minusom
        }

        #endregion Methods
    }
}