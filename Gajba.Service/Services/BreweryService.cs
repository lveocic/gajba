﻿using Gajba.Common.FilterParameters;
using Gajba.Model.Common.Models;
using Gajba.Repository.Common.Repositories;
using Gajba.Service.Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gajba.Service.Services
{
    public class BreweryService : IBreweryService //klasa BreweryService implementira IBreweryService interface
    {
        #region Constructors

        public BreweryService(IBreweryRepository breweryRepository, IUnitOfWork uow, IBeerService beerService)
        //konstruktoru BreweryService predajemo objekte i instanciramo ih
        {
            BreweryRepository = breweryRepository;
            Uow = uow;
            BeerService = beerService;
        }

        #endregion Constructors

        #region Properties

        protected IBeerService BeerService { get; private set; } //protected je jer mu nitko ne moze pristupiti, a set je private jer se ne smije mijenjati
        protected IBreweryRepository BreweryRepository { get; private set; } //protected je jer mu nitko ne moze pristupiti, a set je private jer se ne smije mijenjati
        protected IUnitOfWork Uow { get; private set; }

        #endregion Properties

        //protected je jer mu nitko ne moze pristupiti, a set je private jer se ne smije mijenjati

        #region Methods

        public async Task<bool> DeleteBreweryAsync(Guid id)
        //asinkrona metoda povratnog tipa bool, predajemo id tipa Guid i brise iz baze
        {
            var brewery = await BreweryRepository.GetAsync(id); //dohvaca objekt po id-u
            if (brewery == null) //ukoliko je objekt null
                return false; //vraca false
            var beerFilter = new BeerFilter(); //ukoliko objekt postoji instancira beer filter
            beerFilter.BreweryIds = new List<Guid>() { id }; //popunjava u filteru breweryIds sa predanim Id-jem
            beerFilter.Page = 1; //filtrira (broj stranica), postavlja da trazimo prvu stranicu
            beerFilter.PageSize = 1000; //filtrira (broj zapisa po stranici), postavlja broj itema na stranici na 1000
            var beers = await BeerService.FindBeersAsnyc(beerFilter); //poziva metodu iz Servicea i predajemo joj filter
            var deleteBeers = await Uow.BeerRepository.DeleteRangeUOWAsync(beers); //poziva metodu iz UOW i predajemo joj isfiltriranu listu za brisanje
            var deleteBrewery = await Uow.BreweryRepository.DeleteAsync(brewery); //poziva metodu iz repositorija i predajemo joj objekt koji treba obrisati
            var result = await Uow.Complete(); //izvrsava spremanje promjena na bazi
            Uow.Dispose(); //cisti memoriju
            return result; //vraca povratno info jeli bilo promjena
        }

        public async Task<IEnumerable<IBrewery>> FindBreweriesAsnyc(IBreweryFilter filter) //pozivam metodu za pronalazak objekta, predajem joj objekt tipa IBreweryFilter
        {
            return await BreweryRepository.FindAsync(filter); //pozivam metodu sa repositoryja predajem joj objekt i ona ga pronalazi
        }

        public async Task<IEnumerable<IBrewery>> GetAllBreweriesAsync() //metoda dohvaca sve objekte sa baze
        {
            var result = await BreweryRepository.GetAllAsync(); //pozivam metodu sa repositoryja i ona dohvaca trazeno
            return result;
        }

        public async Task<IBrewery> GetBreweryAsync(Guid id) //metoda dohvaca jedan objekt po id-u tipa Guid
        {
            var result = await BreweryRepository.GetAsync(id); //pozivam metodu sa repositorija i ona dohvaca trazeno
            return result;
        }

        public async Task<bool> InsertBreweryAsync(IBrewery brewery) //metoda ubacuje na bazu objekt tipa IBrewery
        {
            CreateBrewery(brewery); //izvrsava se metoda CreateBrewery sa predanim objektom
            var result = await BreweryRepository.AddAsync(brewery); //pozivam metodu sa repositoryja i ona odraduje dalje
            return result; //ukoliko je sve ok, vraca true
        }

        public async Task<bool> UpdateBreweryAsync(IBrewery brewery) //metoda azurira objekte, predajemo joj objekt tipa IBrewery
        {
            brewery.DateModified = DateTime.Now; //postavlja vrijeme azuriranja
            var result = await BreweryRepository.UpdateAsync(brewery); //pozivam metodu iz repositoryja i ona azurira objekt
            return result; //vraca nam povratno true ili false
        }

        private void CreateBrewery(IBrewery brewery) //metoda je private jer se ne smije mijenjati, predajemo joj objekt tipa IBeer
        {
            brewery.DateCreated = DateTime.UtcNow; //upisuje vrijeme kreiranja objekta
            brewery.DateModified = DateTime.UtcNow; //upisuje vrijeme azuriranja objekta
            brewery.Id = Guid.NewGuid(); //dodjeljuje id objektu
            brewery.Abrv = brewery.Name.ToLower().Replace(" ", "-").Replace("č", "c").Replace("ć", "c").Replace("ž", "z").Replace("š", "s").Replace("đ", "d"); //postavlja abrv, prebacuje sve u mala slova, uklanja palatale i mice razmak minusom
        }

        #endregion Methods
    }
}