﻿using Autofac;
using Gajba.Service.Common.Services;
using Gajba.Service.Services;

namespace Gajba.Service
{
    public class DIModule : Module
    {
        #region Methods

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<BeerService>().As<IBeerService>();
            builder.RegisterType<BreweryService>().As<IBreweryService>();
        }

        #endregion Methods
    }
}