﻿using AutoMapper;
using Gajba.Model.Common.Models;
using Gajba.Model.Models;
using static Gajba.API.Controllers.BeerController;
using static Gajba.API.Controllers.BreweryController;

namespace Gajba.API.Infrastructure
{
    public class RestMapping : Profile
    {
        #region Constructors

        public RestMapping()
        {
            //Beer
            CreateMap<Beer, BeerRestModel>().ReverseMap();
            CreateMap<BeerRestModel, IBeer>().ReverseMap();
            //Brewery
            CreateMap<Brewery, BreweryRestModel>().ReverseMap();
            CreateMap<BreweryRestModel, IBrewery>().ForMember(x => x.Beers, opt => opt.Ignore());
            CreateMap<IBrewery, BreweryRestModel>();
        }

        #endregion Constructors
    }
}