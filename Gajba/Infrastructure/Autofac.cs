﻿using Autofac;
using AutoMapper;
using Gajba.Repository.Mapping;
using System.Reflection;
using System.Threading.Tasks;

namespace Gajba.Infrastructure
{
    public static class AutofacExtensions
    {
        #region Methods

        public static void RegisterAutoMapper(this ContainerBuilder containerBuilder, IEnumerable<Assembly> assemblies)
        {
            //var profiles = assemblies.SelectMany(x => x.GetTypes())
            //    .Where(x => typeof(Profile).IsAssignableFrom(x))
            //    .ToArray();

            containerBuilder.Register(ctx => new MapperConfiguration(cfg =>
            {
                cfg.ConstructServicesUsing(ctx.Resolve);
                cfg.AllowNullCollections = true;
                cfg.AddProfile<EntityToDomainMapping>();
            }))
            .As<AutoMapper.IConfigurationProvider>()
            .SingleInstance();

            containerBuilder.Register(ctx =>
            {
                var config = ctx.Resolve<AutoMapper.IConfigurationProvider>();
                return new Mapper(config, ctx.Resolve);
            })
            .As<IMapper>()
            .SingleInstance();
        }

        #endregion Methods
    }
}