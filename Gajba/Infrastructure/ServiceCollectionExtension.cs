﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Gajba.Infrastructure
{
    public static class ServiceCollectionExtensions
    {
        #region Methods

        public static Autofac.IContainer AddAutofacResolver(this IServiceCollection services)
        {
            var path = AppDomain.CurrentDomain.BaseDirectory;
            var assemblies = Directory.GetFiles(path, "Gajba.*.dll").Select(Assembly.LoadFrom).ToArray();

            services.AddAutoMapper(assemblies);

            var builder = new ContainerBuilder();
            builder.Populate(services);

            // register DIModules
            builder.RegisterAssemblyModules(assemblies);

            // register AutoMapper Profiles
            builder.RegisterAutoMapper(assemblies);
            var container = builder.Build();

            return container;
        }

        #endregion Methods
    }
}