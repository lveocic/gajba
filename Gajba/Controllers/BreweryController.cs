﻿using AutoMapper;
using Gajba.Common.FilterParameters;
using Gajba.Model.Common.Models;
using Gajba.Model.Models;
using Gajba.Service.Common.Services;
using Gajba.Service.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Build.Framework;
using static Gajba.API.Controllers.BeerController;

namespace Gajba.API.Controllers
{
    [Route("api/[controller]")] //putanja
    [ApiController]
    public class BreweryController : ControllerBase //klasa BreweryController nasljeduje AspNetCore predefiniranu klasu
    {
        #region Constructors

        public BreweryController(IBreweryService breweryService, IMapper mapper) //BreweryController konstruktoru predajem BreweryService objekt tipa IBreweryService gdje se nalaze metode, predajem mu mappera koji mapira iz klase u klasu (u ovom slucaju iz klase BreweryRestModel u Brewery Model)
        {
            BreweryService = breweryService;
            Mapper = mapper;
        }

        #endregion Constructors

        #region Properties

        public IBreweryService BreweryService { get; set; }
        public IMapper Mapper { get; set; }

        #endregion Properties

        #region Methods

        // DELETE api/<BreweryController>/5
        [HttpDelete("{id}")]
        public async Task<bool> Delete(Guid id) //Delete endpoint metoda za brisanje pivovare iz baze, asinkrona metoda sa bool povratnom informacijom, predajemo joj id objekta tipa Guid
        {
            if (id == Guid.Empty) //provjeravam sa if uvjetom jel nam id razlicit od guid empty (sve nule), ako je vraca false
                return false;
            return await BreweryService.DeleteBreweryAsync(id); //ako postoji objekt sa predanim id-jem obrisat će ga putem metode iz servicea, i dobit ću povratnu informaciju true
        }

        [HttpGet("filter")]
        public async Task<IEnumerable<BreweryRestModel>> FindAsync(string ids = "", string searchPhrase = "", int? page = 1, int? pageSize = 10) //metoda koja filtrira podatke u bazi, predajemo joj parametre za filtriranje
        {
            var filter = new BreweryFilter(); //instanciram objekt iz klase BreweryFilter
            filter.SearchQuery = searchPhrase; //pretražujem
            filter.Page = page; //definiram broj stranice
            filter.PageSize = pageSize; //broj stavki po stranici
            filter.Ids = !String.IsNullOrWhiteSpace(ids) ? ids.Split(",").Select(x => new Guid(x)) : new List<Guid>();
            // ovo je ternarni operator, prvo provjeravam jel string null i ima li vrijednost,
            // ako ima vrijednost onda string razdvajam po zarezu
            // i kreiram listu Guida, ako nema vrijednost onda stvaram praznu listu
            var result = await BreweryService.FindBreweriesAsnyc(filter); //pozivam metodu iz servicea i predajem joj filter
            if (result != null) //provjeravam jel isfiltrirana lista null, ako nije onda ju predajem mapperu koji ju mapira u listu BreweryRestModela
            {
                return Mapper.Map<List<BreweryRestModel>>(result);
            }
            return null; //ako je null, vraca praznu listu
        }

        // GET: api/<BreweryController>
        [HttpGet]
        public async Task<IEnumerable<BreweryRestModel>> GetBreweries() //metoda za dohvaćanje svih objekata tipa BreweryRestModel, povratni tip je IEnumerable zato što prosljeđujem podatke dalje kroz kod
        {
            var result = await BreweryService.GetAllBreweriesAsync(); //poziva iz servicea metodu GetAllBreweriesAsync koja dohvaća sve objekte u bazi
            return Mapper.Map<List<BreweryRestModel>>(result); //metoda je return tipa i vraća taj određeni objekt koji ima taj id koji smo predali
        }

        // GET api/<BreweryController>/5
        [HttpGet("{id}")]
        public async Task<BreweryRestModel> GetBrewery(Guid id) //metoda koja dohvaća jedan objekt tipa BreweryRestModel po id-u koji joj predamo tipa Guid
        {
            var result = await BreweryService.GetBreweryAsync(id); //poziva kroz Service metodu koja dohvaća taj jedan objekt po id-u
            return Mapper.Map<BreweryRestModel>(result); //metoda je return tipa i vraća taj određeni objekt koji ima id koji smo predali
        }

        // POST api/<BreweryController>
        [HttpPost]
        public async Task<bool> Post([FromBody] BreweryRestModel brewery) //metoda u kojoj unosimo novi objekt tipa BreweryRestModel, FromBody je zato što mu unosimo podatke kroz tijelo http requesta
        {
            if (ValidateBrewery(brewery)) //ukoliko jesmo unjeli sve informacije koje od nas traži klasa BreweryRestModel dobit cemo povratnu informaciju true
            {
                var model = Mapper.Map<Brewery>(brewery); //mapper mapira BreweryRestModel u BreweryModel kako bi service mogao raditi s tim podacima
                return await BreweryService.InsertBreweryAsync(model); //pozivamo metodu iz Servicea koja nam omogucava unos novog objekta u bazu
            }
            return false; //ukoliko nisu ispravne informacije kroz metodu ValidateBrewery dobit cemo povratnu informaciju false
        }

        // PUT api/<BreweryController>/5
        [HttpPut("{id}")]
        public async Task<bool> PutBrewery(Guid id, [FromBody] BreweryRestModel brewery) //metoda za uredivanje vec unesenog objekta tipa BreweryRestModel i sa ranije dodjeljenim id-jem tipa Guid, FromBody je zato što unosimo zahtjev putem http requesta
        {
            if (id != brewery.Id) //provjeravam jel id vec unesen, ako je id drugaciji od id-jeva ranije unesenih vraca false
            {
                return false;
            }
            return await BreweryService.UpdateBreweryAsync(Mapper.Map<Brewery>(brewery)); //ukoliko smo sve potrebno unjeli ispravno, id koji smo naveli u zahtjevu postoji u bazi, dobivamo povratnu informaciju true, i pozivamo metodu iz servicea koja ažurira objekt
        }

        private bool ValidateBrewery(BreweryRestModel brewery) //metoda koja sluzi kao osigurac da su svi podatci uneseni ispravno, predajemo joj objekt tipa BreweryRestModel
        {
            if (brewery == null) //provjerava jel brewery null, ukoliko je, vraća false
            {
                return false;
            }
            else if (brewery.Id == null || brewery.Id == Guid.Empty) //provjerava jel id objekta null ili nije unesen vraca false
            {
                return false;
            }
            else if (string.IsNullOrWhiteSpace(brewery.Name)) //ukoliko je naziv objekta null ili nije unesen vraca false
            {
                return false;
            }
            else if (brewery.Beers == null || brewery.Country == null) //ukoliko jedan od uvjeta nije zadovoljen vraca null
            {
                return false;
            }
            return true; //ako su svi uvjeti gore zadovoljeni vraca true
        }

        #endregion Methods

        #region Classes

        public class BreweryRestModel
        {
            #region Properties

            public string Abrv { get; set; } //naziv objekta koji nikada ne mijenjamo
            public string Address { get; set; } //informacije o objektu

            public List<BeerRestModel>? Beers { get; set; } //Lista objekata
            public string Country { get; set; } //info o objektu
            public DateTime DateCreated { get; set; } //vrijeme kad smo kreirali objekt
            public DateTime DateModified { get; set; } //vrijeme kad smo zadnji puta uređivali objekt
            public string Est { get; set; } //info o objektu
            public Guid Id { get; set; } //Primary Key
            public string Name { get; set; }

            #endregion Properties

            //naziv objekta
        }

        #endregion Classes
    }
}