﻿using AutoMapper;
using Gajba.Common.FilterParameters;
using Gajba.Model.Common.Models;
using Gajba.Model.Models;
using Gajba.Service.Common.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Gajba.API.Controllers
{
    [Route("api/[controller]")] //putanja
    [ApiController]
    public class BeerController : ControllerBase //klasa BeerController nasljeđuje ControlerBase predefiniranu klasu Microsoft.AspNetCorea
    {
        #region Constructors

        public BeerController(IBeerService beerService, IMapper mapper) //BeerController konstruktoru predajem beerService objekt tipa IBeerService gdje se nalaze metode, predajem mu mappera koji mapira iz klase u klasu (u ovom slućaju iz klase BeerRestModel u Beer Model)
        {
            BeerService = beerService;
            Mapper = mapper;
        }

        #endregion Constructors

        #region Properties

        public IBeerService BeerService { get; set; }
        public IMapper Mapper { get; set; }

        #endregion Properties

        #region Methods

        // DELETE api/<BeerController>/
        [HttpDelete("{id}")]
        public async Task<bool> Delete(Guid id) //Delete endpoint metoda za brisanje piva iz baze, asinkrona metoda sa bool povratnom informacijom, predajemo mu id objekta tipa Guid
        {
            if (id == Guid.Empty) //provjeravam sa if uvjetom jel nam id razlicit od guid empty (sve nule), ako je vraca false
                return false;

            return await BeerService.DeleteBeerAsync(id); //ako postoji objekt sa predanim id-jem obrisat će ga putem metode iz servicea, i dobit ću povratnu informaciju true
        }

        //GET api/<BeerController>/filter
        [HttpGet("filter")]
        public async Task<IEnumerable<BeerRestModel>> FindAsync(string ids = "", string searchPhrase = "", bool? isCraft = null, int? page = 1, int? pageSize = 10) //metoda koja filtrira podatke u bazi, predajemo joj parametre za filtriranje
        {
            var filter = new BeerFilter(); //instanciram objekt iz klase BeerFilter
            filter.SearchQuery = searchPhrase; //pretražujem
            filter.IsCraft = isCraft; // dodatne informacije za filter
            filter.Page = page; //broj stranice
            filter.PageSize = pageSize; //broj stavki po stranici
            filter.Ids = !String.IsNullOrWhiteSpace(ids) ? ids.Split(",").Select(x => new Guid(x)) : new List<Guid>();
            // ovo je ternarni operator, prvo provjeravam jel string null i ima li vrijednost,
            // ako ima vrijednost onda string razdvajam po zarezu
            //i kreiram listu Guida, ako nema vrijednost onda stvaram praznu listu
            var result = await BeerService.FindBeersAsnyc(filter); //pozivam metodu iz servicea i predajem joj filter
            if (result != null) //provjeravam jel isfiltrirana lista null, ako nije onda ju predajem mapperu koji ju mapira u listu BeerRestModela
            {
                return Mapper.Map<List<BeerRestModel>>(result);
            }
            return new List<BeerRestModel>(); //ako je null, vraca praznu listu
        }

        // GET api/<BeerController>/
        [HttpGet("{id}")]
        public async Task<BeerRestModel> GetBeer(Guid id) //metoda koja dohvaća jedan objekt tipa BeerRestModel po id-u koji joj predamo tipa Guid
        {
            var result = await BeerService.GetBeerAsync(id); //poziva kroz Service metodu koja dohvaca taj jedan objekt po id-u
            return Mapper.Map<BeerRestModel>(result); //metoda je return tipa i vraca taj odredeni objekt koji ima taj id koji smo predali
        }

        // GET: api/<BeerController>
        [HttpGet]
        public async Task<IEnumerable<BeerRestModel>> GetBeers() //metoda za dohvaćanje svih objekata tipa BeerRestModel, povratni tip je IEnumerable zato što prosljeđujem podatke dalje kroz kod
        {
            var result = await BeerService.GetAllBeersAsync(); //poziva iz servicea metodu GetAllBeersAsync koja dohvaća sve objekte u bazi
            if (result != null) //provjeravam jesu li dohvaceni podaci s baze, ako jesu onda ih predajem mapperu koji ih mapira u listu BeerRestModel
            {
                return Mapper.Map<List<BeerRestModel>>(result);
            }
            return new List<BeerRestModel>(); //ako je null, vraca praznu listu
        }

        // POST api/<BeerController>
        [HttpPost]
        public async Task<bool> Post([FromBody] BeerRestModel beer) //metoda u kojoj unosimo novi objekt tipa BeerRestModel, FromBody je zato sto mu unosimo podatke kroz tijelo http requesta
        {
            if (!ValidateBeer(beer)) //ukoliko nismo unjeli sve informacije koje od nas trazi klasa BeerRestModel dobit cemo povratnu informaciju false
            {
                return false;
            }
            var model = Mapper.Map<Beer>(beer); //mapper mapira BeerRestModel u BeerModel kako bi service mogao raditi s tim podacima
            return await BeerService.InsertBeerAsync(model); //pozivamo metodu iz Servicea koja nam omogucava unos novog objekta u bazu
        }

        // PUT api/<BeerController>/5
        [HttpPut("{id}")]
        public async Task<bool> PutBeer(Guid id, [FromBody] BeerRestModel beer) //metoda za uredivanje vec unesenog objekta tipa BeerRestModel i sa ranije dodjeljenim id-jem tipa Guid, FromBody je zato što unosimo zahtjev putem http requesta
        {
            if (beer == null || id == Guid.Empty) //provjeravam jel unesen objekt i id, ako nije vraca false
                return false;
            var model = Mapper.Map<Beer>(beer); //mapper mapira predani RestModel u Model
            return await BeerService.UpdateBeerAsync(model); //ukoliko smo sve potrebno unjeli ispravno dobivamo povratnu informaciju true, i pozivamo metodu iz servicea koja ažurira objekt
        }

        private bool ValidateBeer(BeerRestModel beer) //metoda koja služi kao osigurac da su svi podatci uneseni ispravno, predajemo joj objekt tipa BeerRestModel
        {
            if (beer == null) //ukoliko je objekt null vraća false
            {
                return false;
            }
            else if (beer.Id == null || beer.Id == Guid.Empty) //ukoliko je id objekta null ili nije unesen vraca false
            {
                return false;
            }
            else if (string.IsNullOrWhiteSpace(beer.Name)) //ukoliko naziv objekta je null ili nije unesen vraca false
            {
                return false;
            }
            else if (beer.BreweryId == null || beer.BreweryId == Guid.Empty) //ukoliko foreign key je null ili nije unesen vraca false
            {
                return false;
            }
            return true; //ukoliko su svi gore navedeni uvjeti zadovoljeni vraca true
        }

        #endregion Methods

        #region Classes

        public class BeerRestModel //klasa BeerRestModel je klasa koju korisnik vidi i koju vracamo nazad korisniku
        {
            #region Properties

            public string Abrv { get; set; } //naziv objekta koji nikad ne mijenjamo
            public int BeerType { get; set; } //informacija o objektu
            public Guid BreweryId { get; set; } //foreign key
            public Guid Id { get; set; } //primary key
            public bool IsCraft { get; set; } //dodatna informacija o objektu
            public string Name { get; set; }

            #endregion Properties

            //naziv objekta
        }

        #endregion Classes
    }
}