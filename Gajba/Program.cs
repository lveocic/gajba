using Autofac;
using Autofac.Extensions.DependencyInjection;
using Gajba.DAL;
using Gajba.Model.Common.Models;
using Gajba.Infrastructure;
using Gajba.Repository.Common.Repositories;
using Gajba.Repository.Repositories;
using Gajba.Service.Common.Services;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using Gajba.Service.Services;
using Gajba.Model.Models;
using AutoMapper;
using Gajba.Repository.Mapping;
using Gajba.API.Infrastructure;
using Gajba.Service;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers(
     options => options.SuppressImplicitRequiredAttributeForNonNullableReferenceTypes = true);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddEntityFrameworkNpgsql().AddDbContext<GajbaContext>(opt =>
        opt.UseNpgsql(builder.Configuration.GetConnectionString("GajbaDb")));
var path = AppDomain.CurrentDomain.BaseDirectory;
var assemblies = Directory.GetFiles(path, "Gajba.*.dll").Select(Assembly.LoadFrom).ToArray();

builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory());
builder.Host.ConfigureContainer<ContainerBuilder>(builder => builder.RegisterModule(new Gajba.Model.DIModule())
                                                                    .RegisterModule(new Gajba.Repository.DIModule())
                                                                    .RegisterModule(new Gajba.Common.DIModule())
                                                                    .RegisterModule(new Gajba.Service.DIModule()));

builder.Services.AddAutoMapper(assemblies);
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();