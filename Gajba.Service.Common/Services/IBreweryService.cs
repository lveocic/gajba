﻿using Gajba.Common.FilterParameters;
using Gajba.Model.Common.Models;
using Gajba.Repository.Common.Repositories;

namespace Gajba.Service.Services
{
    public interface IBreweryService
    {
        #region Methods

        Task<bool> DeleteBreweryAsync(Guid id);

        Task<IEnumerable<IBrewery>> FindBreweriesAsnyc(IBreweryFilter filter);

        Task<IEnumerable<IBrewery>> GetAllBreweriesAsync();

        Task<IBrewery> GetBreweryAsync(Guid id);

        Task<bool> InsertBreweryAsync(IBrewery brewery);

        Task<bool> UpdateBreweryAsync(IBrewery brewery);

        #endregion Methods
    }
}