﻿using Gajba.Common.FilterParameters;
using Gajba.Model.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gajba.Service.Common.Services
{
    public interface IBeerService
    {
        #region Methods

        Task<bool> DeleteBeerAsync(Guid id);

        Task<IEnumerable<IBeer>> FindBeersAsnyc(IBeerFilter filter);

        Task<IEnumerable<IBeer>> GetAllBeersAsync();

        Task<IBeer> GetBeerAsync(Guid id);

        Task<bool> InsertBeerAsync(IBeer beer);

        Task<bool> UpdateBeerAsync(IBeer beer);

        #endregion Methods
    }
}