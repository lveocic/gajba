﻿using Gajba.Common.FilterParameters;
using Gajba.DAL.Entities;
using Gajba.Model.Common.Models;
using Gajba.Model.Models;
using Gajba.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gajba.Repository.Common.Repositories
{
    public interface IBeerRepository : IRepository<BeerEntity, IBeer, IBeerFilter>
    {
    }
}