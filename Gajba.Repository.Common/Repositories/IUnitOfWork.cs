﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gajba.Repository.Common.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        #region Properties

        IBeerRepository BeerRepository { get; }
        IBreweryRepository BreweryRepository { get; }

        #endregion Properties

        #region Methods

        Task<bool> Complete();

        #endregion Methods

    }
}