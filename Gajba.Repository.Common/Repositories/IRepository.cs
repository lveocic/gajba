﻿using Gajba.Common.FilterParameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Gajba.Repository.Repositories
{
    public interface IRepository<T, TModel, TFilter> where T : class where TModel : class where TFilter : IFilter
    {
        #region Methods

        Task<bool> AddAsync(TModel entity);

        Task<bool> AddUOWAsync(TModel entity);

        Task<bool> AddUOWRangeAsync(IEnumerable<TModel> entities);

        Task<bool> DeleteAsync(TModel entity);

        Task<bool> DeleteRangeUOWAsync(IEnumerable<TModel> entities);

        Task<bool> DeleteUOWAsync(TModel entity);

        Task<IEnumerable<TModel>> FindAsync(TFilter filter);

        Task<IEnumerable<TModel>> GetAllAsync();

        Task<TModel> GetAsync(Guid id);

        Task<bool> UpdateAsync(TModel entity);

        Task<bool> UpdateUOWAsync(TModel entity);

        #endregion Methods
    }
}