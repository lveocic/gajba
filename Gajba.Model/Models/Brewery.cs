﻿using Gajba.Model.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gajba.Model.Models
{
    public class Brewery : IBrewery //klasa Brewery je klasa s kojom radi service, implementira IBrewery interface
    {
        #region Properties

        public string Abrv { get; set; }
        public string Address { get; set; }
        public IEnumerable<IBeer> Beers { get; set; }
        public string Country { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public string Est { get; set; }
        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion Properties
    }
}