﻿using Gajba.Model.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gajba.Model.Models
{
    public class Beer : IBeer //klasa Beer je model s kojim radi service, implementira IBeer interface
    {
        #region Properties

        public string Abrv { get; set; }
        public int BeerType { get; set; }
        public IBrewery Brewery { get; set; }
        public Guid BreweryId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public Guid Id { get; set; }
        public bool IsCraft { get; set; }
        public string Name { get; set; }

        #endregion Properties
    }
}