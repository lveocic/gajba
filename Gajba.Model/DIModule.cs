﻿using Autofac;
using Gajba.Model.Common.Models;
using Gajba.Model.Models;

namespace Gajba.Model
{
    public class DIModule : Module //klasa DIModule omogucuje Dependency Injection i nasljeduje Autofac predefiniranu klasu
    {
        #region Methods

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Beer>().As<IBeer>();
            builder.RegisterType<Brewery>().As<IBrewery>();
        }

        #endregion Methods
    }
}