﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gajba.Common.FilterParameters
{
    public interface IBeerFilter : IFilter
    {
        #region Properties

        IEnumerable<Guid> BreweryIds { get; set; }
        bool? IsCraft { get; set; }

        #endregion Properties
    }
}