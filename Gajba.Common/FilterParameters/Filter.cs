﻿namespace Gajba.Common.FilterParameters
{
    public class Filter : IFilter //Filter klasa koja nam je potrebna za filtriranje podataka u bazi, definiramo joj propertije s kojima ćemo filtrirat podatke, implementira IFilter interface
    {
        #region Properties

        public IEnumerable<Guid> Ids { get; set; } //Lista Idijeva (Primary Keyeva) tipa Guid
        public string OrderBy { get; set; } //filtriranje redosljeda
        public string OrderDirection { get; set; } //filtriranje redosljeda
        public int? Page { get; set; } //broj stranica
        public int? PageSize { get; set; } //broj zapisa po stranici
        public string SearchQuery { get; set; }

        #endregion Properties

        //pretraga
    }
}