﻿namespace Gajba.Common.FilterParameters
{
    public class BeerFilter : Filter, IBeerFilter //klasa BeerFilter nasljeduje klasu Filter i implementira IBeerFilter interface
    {
        #region Properties

        public IEnumerable<Guid> BreweryIds { get; set; } //Lista idijeva objekta tipa Guid, IEnumerable je zbog toga sto se ne radi direktno na bazi
        public bool? IsCraft { get; set; }

        #endregion Properties

        //informacija o objektu koja moze biti nullable
    }
}