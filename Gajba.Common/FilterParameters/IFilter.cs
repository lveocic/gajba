﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gajba.Common.FilterParameters
{
    public interface IFilter
    {
        #region Properties

        IEnumerable<Guid> Ids { get; set; }
        string OrderBy { get; set; }
        string OrderDirection { get; set; }
        int? Page { get; set; }
        int? PageSize { get; set; }
        string SearchQuery { get; set; }

        #endregion Properties
    }
}