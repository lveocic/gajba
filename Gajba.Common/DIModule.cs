﻿using Autofac;
using Gajba.Common.FilterParameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gajba.Common
{
    public class DIModule : Module //klasa koja omogucuje Dependency Injection, nasljeduje Module klasu koja je predefinirana
    {
        #region Methods

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<BeerFilter>().As<IBeerFilter>();
            builder.RegisterType<BreweryFilter>().As<IBreweryFilter>();
            builder.RegisterType<Filter>().As<IFilter>();
        }

        #endregion Methods
    }
}